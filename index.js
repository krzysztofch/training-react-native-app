import {AppRegistry} from 'react-native';
import ConnectedApp from './ConnectedApp';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => ConnectedApp);