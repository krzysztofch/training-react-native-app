import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { store } from './src/redux/store';
import App from './App';

const ConnectedApp = () => (
    <Provider store={store}>
        <App />
    </ Provider>
)

export default ConnectedApp;