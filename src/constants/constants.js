export const dataBaseConstants = {
    DB_NAME: 'RNTrainingAppDB.db',
    APP_STATE_TABLE_NAME: 'app_logged_in_state_table',
    USERS_TABLE_NAME: 'users_table',
    DATA_TABLE_NAME: 'data_table'
}