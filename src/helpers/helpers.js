export function isRegisterFormValid (username, password, confirmPassword) {
    let isFormValid = true;
    if(!username || !password || !confirmPassword) {
        isFormValid = false;  
        return isFormValid;      
    } 

    if (password != confirmPassword) {
        isFormValid = false;  
        return isFormValid; 
    }

    return isFormValid;
}

export function isLoginFormValid (username, password) {
    let isFormValid = true;

    if(!username || !password) {
        isFormValid = false;  
        return isFormValid;      
    } 
    
    return isFormValid;
}

export function isAddNewPointFormValid (title, description, latitude, longitude) {
    let isFormValid = true;

    if(!title || !latitude || !longitude) {
        isFormValid = false;  
        return isFormValid;      
    } 

    if(isNaN(Number(latitude)) || isNaN(Number(longitude))) {
        isFormValid = false;  
        return isFormValid;
    }

    return isFormValid; 
}