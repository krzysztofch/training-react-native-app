import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Picker } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { mapActions } from '../../redux/actions/actions';

class SearchScreen extends Component {

    static navigationOptions = {
        title: 'Search'  
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedValue: null
        }
    }

    render() {
        const { points } = this.props.data;   
        
        let dropdownOpitons = points.map(point => {
                return <Picker.Item                       
                            key={point.data_id}
                            label={point.title} 
                            value={point} 
                        />
        });                

        dropdownOpitons.unshift(<Picker.Item                       
                                    key={"initial value"}
                                    label={"Search for locations"} 
                                    value={undefined}                                     
                                />
                        );

        return (
            <View style={styles.container}>                      
                <Picker
                    style={styles.dropdown__list}
                    onValueChange={(itemValue, itemIndex) => {
                        const { dispatch } = this.props;
                        dispatch(mapActions.setCentre(itemValue.latitude, itemValue.longitude, ));
                        this.props.navigation.navigate('Home');  
                    }}
                >
                    { dropdownOpitons}
                </Picker>
            </View>
        )
    }   
};

function mapStateToProps ({ data }) {
    return {
        data
    };
}

var connectedSearchScreen = connect(mapStateToProps)(SearchScreen);

export { connectedSearchScreen as SearchScreen }; 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',              
    },   
    dropdown__list: {
        width: '100%',
        height: 30
    }
});