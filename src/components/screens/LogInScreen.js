import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
import { TextInputComponent } from '../ui/TextInputComponent';
import { loginFormActions } from '../../redux/actions/actions';
import { isLoginFormValid } from '../../helpers/helpers';
import { dataBaseService } from '../../services/dataBaseService';
import { authActions, dataActions } from '../../redux/actions/actions'

class LogInScreen extends Component {
    constructor(props) {
        super(props);
        
        this.handleChange = this.handleChange.bind(this);
        this.handleLinkPress = this.handleLinkPress.bind(this);
        this.handleButtonPress = this.handleButtonPress.bind(this);
        this.handleAppLaunching = this.handleAppLaunching.bind(this); 
    }

    componentDidMount() {
        dataBaseService.log();
        const { dispatch } = this.props;
        dispatch(authActions.loginRequest());
        dataBaseService.checkLoggedInState(this.handleAppLaunching);         
    }

    handleAppLaunching(loggedIn, user, data) {
        const { dispatch, navigation } = this.props;
        if(!loggedIn) { 
            dispatch(authActions.setLoggedIn(loggedIn));
            SplashScreen.hide();
        }  else {
            dispatch(authActions.setLoggedInUser(user));
            dispatch(dataActions.setData(data));
            navigation.navigate('LoggedIn');
            SplashScreen.hide();            
        }
    } 

    handleChange(name, value) {
        const { dispatch } = this.props;
        dispatch(loginFormActions.updateValue(name, value));
    }

    handleButtonPress() {
        const { dispatch, navigation } = this.props;     
        const { username, password } = this.props.loginForm;

        dispatch(loginFormActions.submit());

        if(isLoginFormValid(username, password)) {
            dispatch(authActions.login(username, password, navigation));
        }
    }

    handleLinkPress() {
        this.props.navigation.navigate('Register');
    }

    render() {
        const { username, password, submitted } = this.props.loginForm;
        const { loginInProgress, loginNotification, loginError } = this.props.auth;
        
        return (
            <View style={styles.container}>
                { loginInProgress && <ActivityIndicator style={styles.spinner} size="large" color="#5492f7"/> }
                { !!loginNotification && <Text style={styles.loginForm__notification}> {loginNotification} </Text> }
                { !!loginError && <Text style={styles.login__error}> {loginError} </Text> }
                <Image 
                    source={require('../../assets/images/user_login.png')}
                    style={styles.login_image}
                />
                <View style={styles.rowContainer}>
                    <Text style={styles.loginForm__label}>Username</Text>
                    <TextInputComponent 
                        name="username"
                        value={username}
                        onChangeText={this.handleChange}
                        style={styles.loginForm__input}
                    />
                    { !username && submitted && <Text style={styles.loginForm__error}> Please enter username </Text>}
                </View>

                <View style={styles.rowContainer}>
                    <Text style={styles.loginForm__label}>Password</Text>                
                    <TextInputComponent 
                        name="password"
                        value={password}
                        onChangeText={this.handleChange}
                        style={styles.loginForm__input}
                        secureTextEntry={true}
                    />
                    { !password && submitted && <Text style={styles.loginForm__error}> Please enter password </Text>}
                </View>

                <TouchableOpacity
                    style={styles.loginForm__button}
                    onPress={this.handleButtonPress}                 
                > 
                    <Text style={styles.loginForm__button__text}>LOG IN</Text>                    
                </TouchableOpacity> 

                <View style={styles.footerContainer}>
                    <Text style={styles.loginForm__registerText}>Don't have account?</Text>
                    <Text style={styles.loginForm__registerLink} onPress={this.handleLinkPress}>Create it!</Text>
                </View>

            </View>
        )
    }
   
};

function mapStateToProps ({ loginForm, auth }) {
    return {
        auth,
        loginForm
    };
}

var connectedLogInScreen = connect(mapStateToProps)(LogInScreen);

export { connectedLogInScreen as LogInScreen }; 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    rowContainer: {
        marginBottom: 30,       
        justifyContent: 'center',
        alignItems: 'center',
    },
    loginForm__notification: {
        marginBottom: 20,
        fontSize: 18,
        color: '#018243',
    },
    login__error: {
        marginBottom: 20,
        fontSize: 18,
        color: '#f91616',
    },
    login_image: {
        width: 100,
        height: 100,
        marginBottom: 40
    },
    loginForm__label: {
        fontSize: 20,
        marginBottom: 10
    },
    loginForm__input: {
        width: 220,
        height: 30,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        paddingTop: 3,
        paddingBottom:3
    },
    loginForm__error: {
        marginTop: 10,
        fontSize: 18,
        color: '#f91616',
    },
    footerContainer: {        
        flexDirection: 'row',
        flexWrap:'wrap',
        marginTop: 15,
        fontSize: 18
    },
    loginForm__registerText: {
       marginRight: 5,
       fontSize: 16 
    },
    loginForm__registerLink: {
       color: '#4286f4',
       fontSize: 16
    },
    loginForm__button: {        
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 30,
        paddingRight: 30,
        backgroundColor: '#5492f7',        
    },
    loginForm__button__text: {
        color: '#fff',
        fontSize: 18,
    },
    spinner: {
        marginBottom: 15
    }
});
  