import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { dataActions } from '../../redux/actions/actions'

class PointDetailsScreen extends Component {

    static navigationOptions = {
        title: 'Details'  
    };

    constructor(props) {
        super(props);
        this.ratingCompleted = this.ratingCompleted.bind(this);
    }

    ratingCompleted (rating, id, user_id) {
        const {dispatch} = this.props;

        dispatch(dataActions.updateRating(rating, id, user_id));
    }

    render() {        
        const { points } = this.props.data; 
        const { user } = this.props.auth;

        let route_id = this.props.navigation.getParam("id");

        let point = points.filter((point) => point.data_id == route_id)[0];        

        return (
            <View style={styles.container}>        
                  
                <AirbnbRating
                    type='custom'
                    count={5}
                    imageSize={40}
                    showRating={false}
                    defaultRating={point.rate}
                    onFinishRating={(rating) => this.ratingCompleted(rating, point.data_id, user.id)}
                />    

                <View style={styles.center}>
                    <Text style={styles.title}> {point.title} </Text>
                    { !!point.description && <Text style={styles.description}> {point.description} </Text>}

                    <TouchableOpacity
                        style={styles.back__button}
                        onPress={() => this.props.navigation.navigate('Home')}                
                    > 
                        <Text style={styles.back__button__text}>CLOSE</Text>                    
                    </TouchableOpacity> 
                </View> 

            </View>
        )
    }
   
};

function mapStateToProps ({ auth, data }) {
    return {
        auth,
        data
    };
}

var connectedPointDetailsScreen = connect(mapStateToProps)(PointDetailsScreen);

export { connectedPointDetailsScreen as PointDetailsScreen }; 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between', 
        alignItems: 'center',
        marginVertical: 50,
        paddingLeft: 40,
        paddingRight: 40        
    },
    center: {
        alignItems: 'center',
    },
    title : {
        fontSize: 28,
        fontWeight: "700",
        marginBottom: 30,
    },
    description: {
        marginBottom: 30,
    },
    back__button: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 30,
        paddingRight: 30,
        marginTop: 20,
        backgroundColor: '#5492f7',       
    },
    back__button__text : {
        color: '#fff',
        fontSize: 18,
    }
});