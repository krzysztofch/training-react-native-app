import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import UserMap from '../ui/UserMap';

class HomeScreen extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { user } = this.props.auth;
        const { points } = this.props.data;
        const { mapCentre } = this.props;

        return (
            <View style={styles.container}>

                <UserMap 
                    points={points}
                    mapCentre={mapCentre}
                    onPress={(id) => {
                        this.props.navigation.navigate("Details", {
                            id: id
                        });
                    }} 
                />

                <View style={styles.logoutContainer}> 
                    <TouchableOpacity
                        onPress={() => { this.props.navigation.navigate('LogOut') }}
                        style={styles.logout__button}
                    >
                        <Image 
                            source={require('../../assets/images/logout.png')}
                            style={styles.logout__button__image}
                        />
                    </TouchableOpacity>
                </View>

                <View style={styles.navBarContainer}>
                    <TouchableOpacity
                        onPress={() => { this.props.navigation.navigate('Add') }}
                        style={styles.navBar__button}
                    >
                        <Image 
                            source={require('../../assets/images/add.png')}
                            style={styles.navBar__button__image}
                        />
                        <Text style={styles.navBar__button__text}>Add</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => { this.props.navigation.navigate('Search') }}
                        style={styles.navBar__button}
                    >
                        <Image 
                            source={require('../../assets/images/search.png')}
                            style={styles.navBar__button__image}
                        />
                        <Text style={styles.navBar__button__text}>Search</Text>
                    </TouchableOpacity>                    
                </View>

            </View>
        )
    }   
};

function mapStateToProps ({ auth, mapCentre, data }) {
    return {
        auth,
        mapCentre,
        data
    };
}

var connectedHomeScreen = connect(mapStateToProps)(HomeScreen);

export { connectedHomeScreen as HomeScreen }; 

const styles = StyleSheet.create({
    container: {        
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',      
    },
    navBarContainer: {
        flexDirection: 'row',
        marginVertical: 35,
        backgroundColor: 'transparent',
        justifyContent: 'center',
    },
    logoutContainer: {
        flexDirection: 'row',
        marginBottom: 390,
        backgroundColor: 'transparent',
        justifyContent: 'flex-end',
    },
    navBar__button: {        
        paddingHorizontal: 10,
        alignItems: 'center',
        marginHorizontal: 30,
    },
    navBar__button__image: {
        width: 45,
        height: 45,
        marginBottom: 18,
        opacity: .45
    },
    logout__button: {
        paddingHorizontal: 10,
        alignItems: 'center',
        marginHorizontal: 10,
    },
    logout__button__image: {
        width: 30,
        height: 30,
        opacity: .45
    },
    navBar__button__text: {
        fontSize: 18
    }
});
  