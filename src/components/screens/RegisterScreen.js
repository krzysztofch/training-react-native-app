import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ActivityIndicator } from 'react-native';
import { TextInputComponent } from '../ui/TextInputComponent';
import { connect } from 'react-redux';
import { registerFormActions } from '../../redux/actions/actions';
import { dbInstance, dataBaseService } from '../../services/dataBaseService';
import { isRegisterFormValid } from '../../helpers/helpers';

class RegisterScreen extends Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleLinkPress = this.handleLinkPress.bind(this);
        this.handleButtonPress = this.handleButtonPress.bind(this);        
    }

    handleChange(name, value) {
        const { dispatch } = this.props;
        dispatch(registerFormActions.updateValue(name, value));
    }

    handleButtonPress() {
        const { dispatch, navigation } = this.props;     
        const { username, password, confirmPassword } = this.props.registerForm;  

        dispatch(registerFormActions.submit());
       
        if(isRegisterFormValid(username, password, confirmPassword)) {            
            dispatch(registerFormActions.register(username, password, navigation));
        }
    }

    handleLinkPress() {
        this.props.navigation.navigate('LogIn');
    }

    render() {
        const { username, password, confirmPassword, submitted, registerInProgress } = this.props.registerForm;

        return (
            <View style={styles.container}>
                { registerInProgress && <ActivityIndicator style={styles.spinner} size="large" color="#5492f7"/>}

                <View style={styles.rowContainer}>
                    <Text style={styles.registerForm__label}>Username</Text>
                    <TextInputComponent 
                        name="username"
                        value={username}
                        onChangeText={this.handleChange}
                        style={styles.registerForm__input}
                    />
                    { !username && submitted && <Text style={styles.registerForm__error}> Please enter username </Text>}
                </ View>

                <View style={styles.rowContainer}>
                    <Text style={styles.registerForm__label}>Password</Text>                
                    <TextInputComponent 
                        name="password"
                        value={password}
                        onChangeText={this.handleChange}
                        style={styles.registerForm__input}
                        secureTextEntry={true}
                    />
                    { !password && submitted && <Text style={styles.registerForm__error}> Please enter password </Text>}
                </View>

                <View style={styles.rowContainer}>
                    <Text style={styles.registerForm__label}>Confirm password</Text>                
                    <TextInputComponent 
                        name="confirmPassword"
                        value={confirmPassword}
                        onChangeText={this.handleChange}
                        style={styles.registerForm__input}
                        secureTextEntry={true}
                    />
                    { !confirmPassword && submitted && <Text style={styles.registerForm__error}> Please confirm password </Text>}
                    { !!confirmPassword && (confirmPassword != password) && submitted && <Text style={styles.registerForm__error}> Confirm password doesn't match </Text>}
                </View>

                <TouchableOpacity
                    style={styles.registerForm__button}
                    onPress={this.handleButtonPress}                 
                > 
                    <Text style={styles.registerForm__button__text}>REGISTER</Text>                    
                </TouchableOpacity> 

                <View style={styles.footerContainer}>
                    <Text style={styles.registerForm__registerText}>Already have account?</Text>
                    <Text style={styles.registerForm__registerLink} onPress={this.handleLinkPress}>Log in!</Text>
                </View>
            </View>
        )
    }   
};

function mapStateToProps ({ registerForm }) {
    return {
        registerForm  
    };
}

var connectedRegisterScreen = connect(mapStateToProps)(RegisterScreen);

export { connectedRegisterScreen as RegisterScreen }; 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    rowContainer: {
        marginBottom: 30,       
        justifyContent: 'center',
        alignItems: 'center',
    },
    registerForm__label: {
        fontSize: 20,
        marginBottom: 10
    },
    registerForm__input: {
        width: 220,
        height: 30,        
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        paddingTop: 3,
        paddingBottom:3
    },
    registerForm__error: {
        marginTop: 10,
        fontSize: 18,
        color: '#f91616',
    },    
    footerContainer: {        
        flexDirection: 'row',
        flexWrap:'wrap',
        marginTop: 15,
        fontSize: 18
    },
    registerForm__registerText: {
       marginRight: 5,
       fontSize: 16 
    },
    registerForm__registerLink: {
       color: '#4286f4',
       fontSize: 16
    },
    registerForm__button: {        
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 30,
        paddingRight: 30,
        backgroundColor: '#5492f7',        
    },
    registerForm__button__text: {
        color: '#fff',
        fontSize: 18,
    }
});
  