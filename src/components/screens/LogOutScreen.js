import React, { Component } from 'react';
import { Text, View, TouchableOpacity, ActivityIndicator, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { authActions } from '../../redux/actions/actions';

class LogOutScreen extends Component {
    constructor(props) {
        super(props);

        this.handleButtonPress = this.handleButtonPress.bind(this);        
    }

    handleButtonPress() {
        const { dispatch, navigation } = this.props;       
        dispatch(authActions.logout(navigation));        
    }

    render() {
        const { logoutInProgress } = this.props.auth;

        return (
             <View style={styles.container}>
                {  logoutInProgress && <ActivityIndicator style={styles.spinner} size="large" color="#5492f7"/> }
                
                <Text style={styles.logout__text}> Do you want to log out? </Text>

                <TouchableOpacity
                    style={styles.logout__button}
                    onPress={this.handleButtonPress}                 
                > 
                    <Text style={styles.logout__button__text}>LOG OUT</Text>                    
                </TouchableOpacity> 

            </View>
        )
    }
   
};

function mapStateToProps ({ auth }) {
    return {
        auth,        
    };
}

var connectedLogOutScreen = connect(mapStateToProps)(LogOutScreen);

export { connectedLogOutScreen as LogOutScreen }; 


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
    logout__text: {
        fontSize: 20,
        paddingBottom: 20
    }, 
    logout__button: {        
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 30,
        paddingRight: 30,
        backgroundColor: '#5492f7',        
    },
    logout__button__text: {
        color: '#fff',
        fontSize: 18,
    },
    spinner: {
        marginBottom: 15
    }
});
  