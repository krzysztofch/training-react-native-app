import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { TextInputComponent } from '../ui/TextInputComponent';
import { TextAreaComponent } from '../ui/TextAreaComponent';
import { addNewPointFormActions } from '../../redux/actions/actions';
import { isAddNewPointFormValid } from '../../helpers/helpers';

class AddPointScreen extends Component {

    static navigationOptions = {
        title: 'Add'  
    };

    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleButtonPress = this.handleButtonPress.bind(this);
    }

    handleChange(name, value) {        
        const { dispatch } = this.props;
        dispatch(addNewPointFormActions.updateValue(name, value));       
    }

    handleButtonPress() {
        const { dispatch, navigation } = this.props;     
        const { title, description, latitude, longitude } = this.props.addNewPointForm;
        const { user } = this.props.auth;

        dispatch(addNewPointFormActions.submit());

        if (isAddNewPointFormValid(title, description, latitude, longitude)) {
            dispatch(addNewPointFormActions.addPoint(user.id, title, description, latitude, longitude, navigation));
        } else {
            dispatch(addNewPointFormActions.setError('Please enter valid values'));
        }
    } 

    render() {
        const { title, description, latitude, longitude, submitted, addPointError } = this.props.addNewPointForm;       

        return (
            <View style={styles.container}>
                { !!addPointError && <Text style={styles.addPoint__error}>{addPointError}</Text>}
                <View style={styles.rowContainer}>
                    <Text style={styles.addPointForm__label}>Title</Text>
                    <TextInputComponent 
                        name="title"
                        value={title}
                        onChangeText={this.handleChange}
                        style={styles.addPointForm__input}
                        placeholder="eg. London"
                    />
                    { !title && submitted && <Text style={styles.addPointForm__error}> Please enter title </Text>}
                </View>

                <View style={styles.rowContainer}>
                    <Text style={styles.addPointForm__textarea__label}>Description</Text>
                    <TextAreaComponent 
                        name="description"
                        value={description}
                        onChangeText={this.handleChange}
                        style={styles.addPointForm__textArea}
                        maxLength={300}
                        containerStyle={styles.textareaContainer}                                               
                    />                    
                </View>

                <View style={styles.rowContainer}>
                    <Text style={styles.addPointForm__label}>Coordinates</Text>
                    <TextInputComponent 
                        name="latitude"
                        value={latitude}
                        onChangeText={this.handleChange}
                        style={styles.addPointForm__input}                        
                        placeholder="Latitude"
                    />
                    { !latitude && submitted && <Text style={styles.addPointForm__error}> Please enter latitude </Text>}

                    <TextInputComponent 
                        name="longitude"
                        value={longitude}
                        onChangeText={this.handleChange}
                        style={styles.addPointForm__input}
                        placeholder="Longitude"
                    />
                    { !longitude && submitted && <Text style={styles.addPointForm__error}> Please enter longitude </Text>}
                </View>

                <TouchableOpacity
                    style={styles.addPointForm__button}
                    onPress={this.handleButtonPress}                 
                > 
                    <Text style={styles.addPointForm__button__text}>ADD</Text>                    
                </TouchableOpacity>   

            </View>
        )
    }
   
};

function mapStateToProps ({ auth, addNewPointForm }) {
    return {
        auth,
        addNewPointForm
    };
}

var connectedAddPointScreen = connect(mapStateToProps)(AddPointScreen);

export { connectedAddPointScreen as AddPointScreen }; 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',        
    },
    rowContainer: {       
        width: '90%',
        marginBottom: 30,
    },
    addPointForm__label: {
        marginBottom: 0,
    },
    addPointForm__textarea__label: {
        marginBottom: 15,
    },
    addPointForm__input: {
        width: '100%',
        height: 50,       
        borderBottomWidth: 1,
        borderBottomColor: '#aaa',
        paddingTop: 3,
        paddingBottom:3
    },
    textareaContainer: {
        padding: 5,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: '#aaa',
        height: 150,
    },
    addPointForm__textArea: {        
        textAlignVertical: 'top',
        height: 140,  
    },
    addPointForm__error: {
        marginTop: 10,
        fontSize: 18,
        color: '#f91616',
    },
    addPointForm__button: {        
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 30,
        paddingRight: 30,
        marginTop: 20,
        backgroundColor: '#5492f7',        
    },
    addPointForm__button__text: {
        color: '#fff',
        fontSize: 18,
    },
    addPoint__error: {
        marginTop: 10,
        fontSize: 18,
        color: '#f91616',        
    }
});