import { createStackNavigator } from 'react-navigation';

import { LogInScreen } from '../screens/LogInScreen';
import { RegisterScreen } from '../screens/RegisterScreen';

export const LoggedOutNavigator = createStackNavigator(
    {   
        LogIn: {
            screen: LogInScreen
        },
        Register: {
            screen: RegisterScreen
        }
    },
    { 
        headerLayoutPreset: 'center'
    } 
);