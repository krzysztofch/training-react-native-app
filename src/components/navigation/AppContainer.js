import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { LoggedOutNavigator } from './LoggedOutNavigator'; 
import { LoggedInNavigator } from './LoggedInNavigator';

export default createAppContainer(createSwitchNavigator(
    {
        LoggedIn: LoggedInNavigator,            
        LoggedOut: LoggedOutNavigator            
    },
    {
        initialRouteName: "LoggedOut" 
    }
));