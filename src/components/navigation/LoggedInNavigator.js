import { createStackNavigator } from 'react-navigation';

import { HomeScreen } from '../screens/HomeScreen';
import { AddPointScreen } from '../screens/AddPointScreen';
import { SearchScreen } from '../screens/SearchScreen';
import { PointDetailsScreen } from '../screens/PointDetailsScreen';
import { LogOutScreen } from '../screens/LogOutScreen';

export const LoggedInNavigator = createStackNavigator(
    {
        Home: {
            screen: HomeScreen
        },
        Add: {
            screen: AddPointScreen
        },
        Search: {
            screen: SearchScreen
        },
        Details: {
            screen: PointDetailsScreen
        },
        LogOut: {
            screen: LogOutScreen
        },     
    },
    { 
        headerLayoutPreset: 'center'
    }   
);