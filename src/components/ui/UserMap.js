import React from 'react';
import { View, StyleSheet } from 'react-native';
import MapView from 'react-native-maps';

const UserMap = ({ points, mapCentre, onPress }) => {
   
    let pointMarkers = points.map(point => (
        <MapView.Marker 
            key={point.data_id} 
            title={point.title}
            coordinate={{
                    latitude: point.latitude,
                    longitude: point.longitude
                }                            
            } 
            onPress={() => onPress(point.data_id) }
        />
    )); 
    
    return (         
        <MapView 
            initialRegion={{
                latitude: mapCentre.latitude,
                longitude: mapCentre.longitude,
                latitudeDelta: mapCentre.latitudeDelta,
                longitudeDelta: mapCentre.longitudeDelta,
            }}
            region={mapCentre}
            style={styles.map} 
        >
            { !!pointMarkers.length && pointMarkers }
        </MapView>
    );
};

export default UserMap;

const styles = StyleSheet.create({    
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});
