import React from 'react';
import Textarea from 'react-native-textarea';

export const TextAreaComponent = ({value, onChangeText, name, ...props}) => (
    <Textarea
        value={value}
        onChangeText={(value) => onChangeText(name, value)}        
        {...props}
    />
)