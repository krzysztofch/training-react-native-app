import SQLite from 'react-native-sqlite-storage';
import { dataBaseConstants } from '../constants/constants';

SQLite.enablePromise(true);

export const dataBaseService = {       
    insertIntoDatabase,
    login,
    logout,
    register,
    log,
    checkLoggedInState,
    addPoint,
    fetchUserData,
    updateRating
};

const dbInstance = SQLite.openDatabase({ name: dataBaseConstants.DB_NAME })
    .then(db => {
        // db.transaction((tx) => {     
        //     tx.executeSql(                    
        //         `DROP TABLE IF EXISTS ${dataBaseConstants.APP_STATE_TABLE_NAME}`,
        //         []                    
        //     ).catch(error => { console.log(error) });

        //     tx.executeSql(
        //         `DROP TABLE IF EXISTS ${dataBaseConstants.USERS_TABLE_NAME}`,
        //         []
        //     ).catch(error => { console.log(error) }); 

        //     tx.executeSql(
        //         `DROP TABLE IF EXISTS ${dataBaseConstants.DATA_TABLE_NAME}`,
        //         []
        //     ).catch(error => { console.log(error)} );                 
        // });

        db.transaction((tx) => {                                    
            tx.executeSql(            
                `CREATE TABLE IF NOT EXISTS ${dataBaseConstants.APP_STATE_TABLE_NAME} (logged_in INT(1), user_id INT(3))`,
                []
            ).then(
                () => {
                    db.transaction(
                        (txn) => {
                            txn.executeSql(
                                `SELECT * FROM ${dataBaseConstants.APP_STATE_TABLE_NAME}`,
                                []
                            ).then(([txn, results]) => {
                                if(results.rows.length == 0) {                                
                                    db.transaction((trxn) => {
                                        trxn.executeSql(
                                            `INSERT INTO ${dataBaseConstants.APP_STATE_TABLE_NAME} VALUES (?,?)`,
                                            [0, null]
                                        )
                                    })
                                }
                            }).catch(error => { console.log(error) }); 
                        }
                    )   
                }
            ).catch(error => { console.log(error) });  

            tx.executeSql(
                `CREATE TABLE IF NOT EXISTS ${dataBaseConstants.USERS_TABLE_NAME}   (
                                                                                        user_id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                                                        username VARCHAR(20),
                                                                                        password  VARCHAR(20)
                                                                                    )`,
                []
            ).catch(error => { console.log(error) });

            tx.executeSql(
                `CREATE TABLE IF NOT EXISTS ${dataBaseConstants.DATA_TABLE_NAME}    (
                                                                                        data_id INTEGER PRIMARY KEY AUTOINCREMENT, 
                                                                                        user_id INT(3),
                                                                                        title VARCHAR(20),
                                                                                        description VARCHAR(300),
                                                                                        latitude FLOAT(24),
                                                                                        longitude FLOAT(24),
                                                                                        rate INT(1)
                                                                                    )`,
                []
            ).catch(error => { console.log(error) });
        })

        return db;
}); 

function insertIntoDatabase(tableName, values, callback) {
    dbInstance
        .then(db => {
            db.transaction((tx) => {
                tx.executeSql(
                    `INSERT INTO ${tableName} (${switchColumns(tableName)}) VALUES (?,?)`,
                    values
                ).then(([txn, results]) => {
                    if(results.rowsAffected && typeof callback == 'function') {
                        callback();
                    }
                }).catch(error => { console.log(error) }); 
            })
        })
}

function register(values, callback) {
    dbInstance
        .then(db => {
            db.transaction((tx) => {
                tx.executeSql(
                    `INSERT INTO ${dataBaseConstants.USERS_TABLE_NAME} (username, password) VALUES (?,?)`,
                    values
                ).then(([txn, results]) => {
                    if(results.rowsAffected && typeof callback == 'function') {
                        callback();
                    }
                }).catch(error => { console.log(error) }); 
            })
        })
}

function log () {
    dbInstance
        .then(db => {
            db.transaction((tx) => {
                tx.executeSql(
                    `SELECT * FROM ${dataBaseConstants.DATA_TABLE_NAME}`,
                    []
                ).then(([txn, results]) => {
                   console.log(results.rows.item(0));
                }).catch(error => { console.log(error) }); 
            }); 

            // db.transaction((tx) => {
            //     tx.executeSql(
            //         `SELECT * FROM ${dataBaseConstants.USERS_TABLE_NAME}`,
            //         []
            //     ).then(([txn, results]) => {
            //        console.log(results.rows.item(0));
            //     }).catch(error => { console.log(error) }); 
            // });
        })
}

function checkLoggedInState (callback) {
    dbInstance
        .then(db => {
            db.transaction(tx => {
                tx.executeSql(
                    `SELECT * FROM ${dataBaseConstants.APP_STATE_TABLE_NAME}`,
                    []
                ).then(([tx, state_results]) => {
                    if (state_results.rows.length > 0) {
                        if(state_results.rows.item(0).logged_in == 0) {   
                            callback(state_results.rows.item(0).logged_in);
                        } else {
                            db.transaction((tx) => {
                                tx.executeSql(
                                    `SELECT * FROM ${dataBaseConstants.USERS_TABLE_NAME} WHERE user_id=?`,
                                    [state_results.rows.item(0).user_id]
                                ).then(([tx, user_results]) => {
                                    db.transaction(
                                        tx => {
                                            tx.executeSql(
                                                `SELECT * FROM ${dataBaseConstants.DATA_TABLE_NAME} WHERE user_id=?`,
                                                [user_results.rows.item(0).user_id]    
                                            ).then(([tx, data_results]) => {
                                                let data_results_temp = [];
    
                                                for (let i = 0; i < data_results.rows.length; i++) {
                                                    data_results_temp.push(data_results.rows.item(i));
                                                }
                                                                            
                                                callback(
                                                    state_results.rows.item(0).logged_in,
                                                    {
                                                        id: user_results.rows.item(0).user_id, 
                                                        name: user_results.rows.item(0).username
                                                    },
                                                    data_results_temp
                                                );
                                            }) 
                                        }
                                    )
                                }).catch(error => { console.log(error) });
                            })
                        }
                    } else {
                        callback(0);
                    }                   
                }).catch(error => { console.log(error) }); 
            })
        })
}

function login (values, callbackSuccess, callbackFailure) {
    dbInstance
        .then(db => {
            db.transaction(tx => {
                tx.executeSql(
                    `SELECT * FROM ${dataBaseConstants.USERS_TABLE_NAME} WHERE username=? AND password=?`,
                    values
                ).then(([tx, user_results]) => {
                    if (user_results.rows.length > 0) {
                        db.transaction(tx => {
                            tx.executeSql(
                                `UPDATE ${dataBaseConstants.APP_STATE_TABLE_NAME} SET logged_in=1, user_id=?`,
                                [user_results.rows.item(0).user_id]
                            ).then(() => {
                                db.transaction(
                                    tx => {
                                        tx.executeSql(
                                            `SELECT * FROM ${dataBaseConstants.DATA_TABLE_NAME} WHERE user_id=?`,
                                            [user_results.rows.item(0).user_id]    
                                        ).then(([tx, data_results]) => {
                                            let data_results_temp = [];

                                            for (let i = 0; i < data_results.rows.length; i++) {
                                                data_results_temp.push(data_results.rows.item(i));
                                            }
                                                                        
                                            callbackSuccess(user_results.rows.item(0).user_id, user_results.rows.item(0).username, data_results_temp);
                                        }) 
                                    }
                                )}
                            )                            
                        });        
                    } else {
                        callbackFailure('Wrong username or password');
                    }
                 }).catch(error => { console.log(error) }); 
            })
        }).catch(error => { console.log(error) });
}

/* Helpers functions */

function switchColumns(tableName) {
    switch (tableName) {
        case dataBaseConstants.USERS_TABLE_NAME:
            return 'username, password';
    } 
}

function logout(callback) {
    dbInstance
        .then(db => {
            db.transaction(tx => {
                tx.executeSql(
                    `UPDATE ${dataBaseConstants.APP_STATE_TABLE_NAME} SET logged_in=0, user_id=''`,
                    []
                ).then(([txn, results])=>{                    
                    if(results.rowsAffected > 0) {
                        callback();
                    }
                })
            })
        })
}

function addPoint (values, callback) {
    dbInstance
        .then(db => {
            db.transaction(tx => {
                tx.executeSql(
                    `INSERT INTO ${dataBaseConstants.DATA_TABLE_NAME} (user_id, title, description, latitude, longitude, rate) VALUES (?,?,?,?,?,?)`,
                    values
                ).then(([txn, results])=>{                    
                    if(results.rowsAffected > 0) {
                        callback();
                    }
                })
            })
        }).catch(error => { console.log(error) }); 
}

function fetchUserData (values, callback) {
    dbInstance
        .then(db => {
            db.transaction(tx => {
                tx.executeSql(
                    `SELECT * FROM ${dataBaseConstants.DATA_TABLE_NAME} WHERE user_id=?`,
                    values
                ).then(([tx, data_results]) => {
                    let data_results_temp = [];
                    for (let i = 0; i < data_results.rows.length; i++) {
                        data_results_temp.push(data_results.rows.item(i));
                    }
                                                
                    callback(data_results_temp);
                })
            })
        }).catch(error => { console.log(error) }); 
}

function updateRating (values, callback) {
    dbInstance
        .then(db => {
            db.transaction(tx => {
                tx.executeSql(
                    `UPDATE ${dataBaseConstants.DATA_TABLE_NAME} SET rate=? WHERE data_id=?`,
                    values
                ).then(([tx, data_results])=>{
                    if(data_results.rowsAffected > 0) {
                        callback();
                    }
                })
            })
        })
}