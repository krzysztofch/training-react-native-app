import { addNewPointFormConstants } from '../constants/constants';

const initialState = { 
    title: '',
    description: '',
    latitude: '',
    longitude: '',
    submitted: false,
    addPointError: '',
};

export function addNewPointForm (state = initialState, action) {
    switch(action.type) {
        case addNewPointFormConstants.SET_TITLE:
            return {
                ...state,
                title: action.payload.title
            };
        case addNewPointFormConstants.SET_DESCRIPTION:
            return {
                ...state,
                description: action.payload.description
            };
        case addNewPointFormConstants.SET_LATITUDE:
            return {
                ...state,
                latitude: action.payload.latitude
            };
        case addNewPointFormConstants.SET_LONGITUDE:
            return {
                ...state,
                longitude: action.payload.longitude
            };
        case addNewPointFormConstants.SUBMIT_FORM:
            return {
                ...state,
                submitted: action.payload.submitted
            };
        case addNewPointFormConstants.SET_ERROR:
            return {
                ...state,
                addPointError: action.payload.error
            };
        case addNewPointFormConstants.RESET_FORM:
            return {
                title: '',
                description: '',
                latitude: '',
                longitude: '',
                submitted: false,
                addPointError: '',
            };
        default: {           
            return state;    
        }
    }
}