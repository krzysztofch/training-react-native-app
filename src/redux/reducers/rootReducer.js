import { combineReducers } from 'redux';
import { auth } from './authReducer';
import { loginForm } from './loginFormReducer';
import { registerForm } from './registerFormReducer';
import { addNewPointForm } from './addNewPointFormReducer';
import { mapCentre } from './mapReducer';
import { data } from './dataReducer';

const rootReducer = combineReducers({
    auth,
    loginForm,
    registerForm,
    addNewPointForm,
    mapCentre,
    data,    
});

export default rootReducer;
