import { loginFormConstants } from '../constants/constants';

const initialState = { 
    username: '',
    password: '',
    submitted: false
};

export function loginForm (state = initialState, action) {
    switch(action.type) {
        case loginFormConstants.SET_USERNAME:
            return {
                ...state,
                username: action.payload.username
            };
        case loginFormConstants.SET_PASSWORD:
            return {
                ...state,
                password: action.payload.password
            };
        case loginFormConstants.SUBMIT_FORM:
            return {
                ...state,
                submitted: action.payload.submitted
            };       
        case loginFormConstants.RESET_LOGIN_FORM:
            return {
                username: '',
                password: '',
                submitted: false                     
            };        
        default: {           
            return state;    
        }
    }
}
