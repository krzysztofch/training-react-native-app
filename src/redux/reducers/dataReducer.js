import { dataConstants } from '../constants/constants';

const initialState = { 
    points: []
}

export function data (state = initialState, action) {
    switch(action.type) {
        case dataConstants.SET_DATA: {
            return {
                ...state,
                points: action.payload.points
            };
        }
        default: {           
            return state;    
        }
    }
}