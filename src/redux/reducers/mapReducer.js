import { mapConstants } from '../constants/constants';

const initialState = { 
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: 0.0422,
    longitudeDelta: 0.0422,
}

export function mapCentre (state = initialState, action) {
    switch(action.type) {
        case mapConstants.SET_MAP_CENTRE: {
            return {
                latitude: action.payload.latitude,
                longitude: action.payload.longitude,
                latitudeDelta: 0.0422,
                longitudeDelta: 0.0422,
            };
        }
        default: {           
            return state;    
        }
    }
}