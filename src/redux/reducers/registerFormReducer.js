import { registerFormConstants } from '../constants/constants';

const initialState = { 
    username: '',
    password: '',
    confirmPassword: '',
    submitted: false,
    registerError: '',
    registerInProgress: false
};

export function registerForm (state = initialState, action) {
    switch(action.type) {
        case registerFormConstants.SET_USERNAME:
            return {
                ...state,
                username: action.payload.username
            };
        case registerFormConstants.SET_PASSWORD:
            return {
                ...state,
                password: action.payload.password
            };
        case registerFormConstants.SET_CONFIRM_PASSWORD:
            return {
                ...state,
                confirmPassword: action.payload.confirmPassword
            };
        case registerFormConstants.SUBMIT_FORM:
            return {
                ...state,
                submitted: action.payload.submitted
            };
        case registerFormConstants.REGISTER_REQUEST:
            return {
                ...state,
                registerInProgress: true
            };       
        case registerFormConstants.REGISTER_REQUEST_SUCCESS:
            return {
                username: '',
                password: '',
                confirmPassword: '',
                submitted: false,
                registerError: '',
                registerInProgress: false
            }
        default: {           
            return state;    
        }
    }
}