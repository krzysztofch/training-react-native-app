import { authConstants } from '../constants/constants';

const initialState = { 
    loginInProgress: false,
    logoutInProgress: false,
    loggedIn: 0,               
    user: {
        id: '',
        name: '',
    },        
    loginError: '',        
    loginNotification: '', 
};

export function auth (state = initialState, action) {    
    switch(action.type) {
        case authConstants.LOGIN_REQUEST:
            return {
                ...state,
                loginInProgress: true,
                loginError: '',        
                loginNotification: '', 
            };
        case authConstants.LOGIN_REQUEST_SUCCESS:
            return {
                ...state,
                username: action.payload.username
            };
        case authConstants.LOGIN_REQUEST_FAILURE:
            return {
                ...state,
                loginInProgress: false,
                loginError: action.payload.error,        
                loginNotification: '', 
            };
        case authConstants.SET_LOGGED_IN: 
            return {
                ...state,
                loginInProgress: false,
                loggedIn: action.payload.loggedIn
            };
        case authConstants.SET_LOGGED_IN_USER:
            return {
                ...state,
                loginInProgress: false,
                loggedIn: 1,       
                user: {
                     id: action.payload.user.id,
                     name: action.payload.user.name,
                }
            };
        case authConstants.LOGOUT_REQUEST: 
            return {
                ...state,
                logoutInProgress: true               
            };        
        case authConstants.LOGOUT_REQUEST_SUCCESS:
            return {
                loginInProgress: false,
                logoutInProgress: false,
                loggedIn: 0,       
                user: {
                    id: '',
                    name: '',
                }
            }
        case authConstants.SET_NOTIFICATION:
            return {
                ...state,
                loginError: '',
                loginNotification: action.payload.message
            };
        default: {           
            return state;    
        }
    }
}
