state = {
    auth: {
        loginInProgress: Boolean,
        logoutInProgress: Boolean,
        loggedIn: Number,               
        user: {
            id: String,
            name: String,
        },        
        loginError: String,        
        loginNotification: String              
    },
    loginForm: {
        username: String,
        password: String,
        submitted: Boolean
    },
    registerForm: {
        username: String,
        password: String,
        confirmPassword: String,
        submitted: Boolean,
        registerError: String,
        registerInProgress: Boolean,
    },
    addNewPointForm: {
        title: String,
        description: String,
        latitude: Number,
        longitude: Number,
        submitted: Boolean,
        addPointError: String,
    },
    mapCentre: {
        latitude: Number,
        longitude: Number,
        latitudeDelta: Number,
        longitudeDelta: Number,
    },
    data: {
        points: [
            {
                id: String,
                title: String,
                description: String,                
                coordinates: {
                    latitude: Number,
                    longitude: Number,
                }
            }
        ]
    }
};