import { mapConstants } from '../constants/constants';

export const mapActions = {
    setCentre
}

function setCentre (latitude, longitude, latitudeDelta, longitudeDelta) {
    return {
        type: mapConstants.SET_MAP_CENTRE,
        payload: {
            latitude,
            longitude,
            latitudeDelta,
            longitudeDelta
        }
    }
}