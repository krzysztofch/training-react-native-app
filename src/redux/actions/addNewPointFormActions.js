import { addNewPointFormConstants } from '../constants/constants';
import { dataBaseService } from '../../services/dataBaseService';
import { dataActions } from './actions';

export const addNewPointFormActions = {
    updateValue,
    submit,
    resetForm,
    addPoint,
    setError  
}

function updateValue (name, value) {
    switch(name) {
        case 'title':
            return {
                type: addNewPointFormConstants.SET_TITLE,
                payload: {
                    [name]: value
                }
            };
        case 'description':
            return {
                type: addNewPointFormConstants.SET_DESCRIPTION,
                payload: {
                    [name]: value
                }
            }
        case 'latitude':
            return {
                type: addNewPointFormConstants.SET_LATITUDE,
                payload: {
                    [name]: value
                }
            };
        case 'longitude':
            return {
                type: addNewPointFormConstants.SET_LONGITUDE,
                payload: {
                    [name]: value
                }
            };
    }
}

function submit() {
    return {
        type: addNewPointFormConstants.SUBMIT_FORM,
        payload: {
            submitted: true
        }
    }
}

function addPoint (user_id ,title, description, latitude, longitude, navigation) {
    return dispatch => {

        let initialRate = 0;

        dataBaseService.addPoint([user_id, title, description, latitude, longitude, initialRate], callbackSuccess);

        function callbackSuccess() {
            dispatch(resetAddPointForm());                 
            dispatch(dataActions.refreshData(user_id));            
            navigation.navigate('Home');
        }         
    }

    function resetAddPointForm () { return { type: addNewPointFormConstants.RESET_FORM }};    
}

function resetForm () {
    return {
        type: loginFormConstants.RESET_LOGIN_FORM
    }
}

function setError (error) {
    return {
        type: addNewPointFormConstants.SET_ERROR,
        payload: {
            error
        }
    }
}
