import { authConstants } from '../constants/constants';
import { dataBaseService } from '../../services/dataBaseService';
import { loginFormActions } from './actions';
import { dataActions } from './actions';

export const authActions = {
    login,
    logout,
    loginRequest,
    setLoggedIn,
    setLoggedInUser,
    setNotification    
}

function login (username, password, navigation) {
    return dispatch => {
        dispatch(request());

        dataBaseService.login([username, password], callbackSuccess, callbackFailure);

        function callbackSuccess(user_id, username, data) {
            dispatch(resetLoginForm());                 
            dispatch(authActions.setLoggedInUser({
                id: user_id,
                name: username
            }));        
            dispatch(dataActions.setData(data));
            navigation.navigate('LoggedIn');            
        }

        function callbackFailure(error) {
            dispatch(requestFailure(error));
        }
    }

    function request () { return authActions.loginRequest()};
    function resetLoginForm () { return loginFormActions.resetForm()};
    function requestSuccess (user) { return { type: authConstants.LOGIN_REQUEST_SUCCESS }};      
    function requestFailure (error) { return { type: authConstants.LOGIN_REQUEST_FAILURE, payload: { error } }};
}

function logout (navigation) {
    return dispatch => {
        dispatch(request());

        dataBaseService.logout(callbackSuccess);

        function callbackSuccess() {
            dispatch(requestSuccess()); 
            navigation.navigate('LoggedOut');            
        } 
    }

    function request () { return {type: authConstants.LOGOUT_REQUEST}};
    function requestSuccess () { return {type: authConstants.LOGOUT_REQUEST_SUCCESS}};
}

function loginRequest () {
    return { 
        type: authConstants.LOGIN_REQUEST 
    }
}

function setLoggedIn (loggedIn) {
    return {
        type: authConstants.SET_LOGGED_IN,
        payload: {
            loggedIn
        }
    }
}

function setLoggedInUser (user) {
    return {
        type: authConstants.SET_LOGGED_IN_USER,
        payload: {
            user
        }
    }
}

function setNotification (message) {
    return {
        type: authConstants.SET_NOTIFICATION,
        payload: {
            message: message
        }
    };
}
