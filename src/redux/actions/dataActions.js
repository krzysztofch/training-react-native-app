import { dataConstants } from '../constants/constants';
import { dataBaseService } from '../../services/dataBaseService';

export const dataActions = {
    setData,
    updateRating,
    refreshData
}

function setData (data) {
    return {
        type: dataConstants.SET_DATA,
        payload: {
            points: data
        }
    }
}

function refreshData (user_id) {
    return dispatch => {

        dataBaseService.fetchUserData([user_id], callbackSuccess);

        function callbackSuccess(data) {
            dispatch(setData(data));
        }
    }
}

function updateRating (rating, id, user_id) {   
    return dispatch => {        
        dataBaseService.updateRating([rating, id], callback);

        function callback () {
            dispatch(refreshData(user_id));
        }
    }    
}