import { loginFormConstants, authConstants } from '../constants/constants';
import { dataBaseService } from '../../services/dataBaseService';
import { authActions } from './actions';

export const loginFormActions = {
    updateValue,
    submit,
    resetForm    
}

function updateValue (name, value) {
    switch(name) {
        case 'username':
            return {
                type: loginFormConstants.SET_USERNAME,
                payload: {
                    [name]: value
                }
            };
        case 'password':
            return {
                type: loginFormConstants.SET_PASSWORD,
                payload: {
                    [name]: value
                }
            }
    }
}

function submit () {
    return {
        type: loginFormConstants.SUBMIT_FORM,
        payload: {
            submitted: true
        }
    }
}

function resetForm () {
    return {
        type: loginFormConstants.RESET_LOGIN_FORM
    }
}


