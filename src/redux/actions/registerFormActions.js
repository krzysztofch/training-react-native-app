import { registerFormConstants } from '../constants/constants';
import { dataBaseConstants } from '../../constants/constants';
import { dataBaseService } from '../../services/dataBaseService';
import { loginFormActions, authActions } from './actions';

export const registerFormActions = {
    updateValue,
    submit,
    register
}

function updateValue (name, value) {
    switch(name) {
        case 'username':
            return {
                type: registerFormConstants.SET_USERNAME,
                payload: {
                    [name]: value
                }
            };
        case 'password':
            return {
                type: registerFormConstants.SET_PASSWORD,
                payload: {
                    [name]: value
                }
            }
        case 'confirmPassword':
            return {
                type: registerFormConstants.SET_CONFIRM_PASSWORD,
                payload: {
                    [name]: value
                }
            }
    }
}

function submit () {
    return {
        type: registerFormConstants.SUBMIT_FORM,
        payload: {
            submitted: true
        }
    }
}

function register (username, password, navigation) {
    return dispatch => {
        dispatch(request());

        dataBaseService.register( [username, password], callbackSuccess);        
        
        function callbackSuccess() {
            dispatch(requestSuccess());
            navigation.navigate('LogIn');
            dispatch(authActions.setNotification('User has been successfully registered'));
        }        
    }

    function request (user) { return { type: registerFormConstants.REGISTER_REQUEST }};
    function requestSuccess (user) { return { type: registerFormConstants.REGISTER_REQUEST_SUCCESS }};
}