import React from 'react';
import CreateAppContainer from './src/components/navigation/AppContainer';

const App = () => (
  <CreateAppContainer />
);

export default App;
